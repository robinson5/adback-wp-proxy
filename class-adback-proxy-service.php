<?php
/**
 * This class is basically a service. It will make AdBack request to get
 * fresh data after 6 minutes, following the documentation guidelines.
 * It uses WP Transient API, instead of Redis in the example from documentation.
 *  
 * The documentation is at https://adback-anti-adblock-solution.github.io/slate/
 * 
 * @author Robinson Lemos <robinson@code6.com.br>
 */
class Adback_Proxy_Service {
    /**
     * Transient API Key
     */
    const ADBACK_TRANSIENT_KEY = 'adback_proxy_service';

    /**
     * Cache time in hours to save data inside transient
     */
    const CACHE_TIME_IN_HOURS = 6;

    /**
     * AdBack API Token
     *
     * @var string
     */
    private $token;

    /**
     * Class constructor
     *
     * @param string $token
     */
    public function __construct($token)
    {
       $this->token = $token;
    }

    /**
     * Make a HTTP request to AdBack, using the specified token.
     *
     * @return array|WP_Error
     */
    protected function makeRequest()
    {
        $http = new WP_Http;

        return $http->get('https://adback.co/api/script/me/full?access_token=' . $this->token);
    }

    /**
     * Returns AdBack data from cache
     *
     * @return array|null
     */
    protected function getFromCache()
    {
        if ($data = get_transient(self::ADBACK_TRANSIENT_KEY)) {
            return unserialize($data);
        }

        return null;
    }

    /**
     * (Re)Populate the cache
     *
     * @param $repopulate bool If we want to repopulate the cache
     * 
     * @return void
     * @throws RuntimeException
     */
    public function populate($repopulate = false)
    {
        // If we got here, then we make an API request, get the response,
        // serialize-it (more efficient than json_encode) and save it inside transient API.
        $request = $this->makeRequest();

        // Tell that is something wrong if we don't have a body :(
        if (!isset($request['body'])) {
            throw new RuntimeException('Whoops. Something went wrong');
        }

        // We need to parse the JSON to get the data, using the assoc mode
        $response = json_decode($request['body'], true);

        // If we have the repopulate flag as true, we delete the
        // transient value
        if ($repopulate) {
            delete_transient(self::ADBACK_TRANSIENT_KEY);
        }

        // Save the data into Transient API
        set_transient(self::ADBACK_TRANSIENT_KEY, serialize($response), self::CACHE_TIME_IN_HOURS * 60 * 60);
    }

    /**
     * Load AdBack API Data. Check if it exists inside Transient API, and it is valid.
     * Otherwise, it will fetch from AdBack API.
     *
     * @return void
     */
    public function load()
    {
        // Here we make the procedure to load AdBack data.
        // We'll follow the guidelines from documentation to cache content. Instead
        // of Redis usage, we'll stick with WordPress Transient API
        // to store data.
        $cache = $this->getFromCache();

        // We need the is_array check to make sure that
        // we have some fetched data, to prevent to make requests that
        // always returns empty from AdBack API.
        if ($cache || is_array($cache)) {
            return $cache;
        }

        // Populate the cache
        $this->populate();

        // After all of that, we return the response with the data we need to work.
        return $this->getFromCache();
    }
}