<?php
/**
 * Plugin Name: AdBack Proxy
 * Description: A helper plugin to configure AdBack Proxy with WordPress
 */

require_once __DIR__ . '/class-adback-proxy-service.php';

if (defined('ADBACK_TOKEN')) {
    function add_adback_scripts_to_footer() {
        // We only work on legit pages.
        if (!is_404()) {
            $adbackClass = new Adback_Proxy_Service(ADBACK_TOKEN);
            $adbackData = $adbackClass->load();

            foreach ($adbackData as $scriptCodes) {
                foreach ($scriptCodes as $scriptCode) {
                    echo "<script>{$scriptCode['code']}</script>";
                }
            }
        }
    }

    add_action('wp_footer', 'add_adback_scripts_to_footer');

    function clear_adback_cache() {
        if (isset($_GET['clear_adback_cache'])) {
            $adbackClass = new Adback_Proxy_Service(ADBACK_TOKEN);
            $adbackClass->populate(true);
        }
    }
    
    add_action('wp', 'clear_adback_cache');
}